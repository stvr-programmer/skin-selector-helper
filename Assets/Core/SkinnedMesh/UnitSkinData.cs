using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Skin Data", fileName = "_skinData")]
public class UnitSkinData : ScriptableObject
{
    [SerializeField] List<SkinData> unitSkinParts = new List<SkinData>();
    Dictionary<string, SkinData> cacheSkin = new Dictionary<string, SkinData>();
    public int skinAmount { get { return unitSkinParts.Count; } }
    public void ChangeSkin(SkinnedMeshRenderer skinedMesh, string skinId)
    {
        if (unitSkinParts.Count <= 0) { InitialData(); }

        if (cacheSkin.ContainsKey(skinId))
        {
            skinedMesh.sharedMesh = cacheSkin[skinId].partMesh;
            skinedMesh.sharedMaterials = cacheSkin[skinId].partMaterials;
        }
    }
    public void ChangeSkin(SkinnedMeshRenderer skinedMesh, int index)
    {
        skinedMesh.sharedMesh = unitSkinParts[index].partMesh;
        skinedMesh.sharedMaterials = unitSkinParts[index].partMaterials;
    }

    private void InitialData()
    {
        foreach (var skin in unitSkinParts)
        {
            cacheSkin.Add(skin.skinId, skin);
        }
    }
}

[System.Serializable]
public struct SkinData
{
    public string skinId;
    public Mesh partMesh;
    public Material[] partMaterials;
}
