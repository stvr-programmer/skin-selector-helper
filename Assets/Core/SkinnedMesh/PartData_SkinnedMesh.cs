using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class PartData_SkinnedMesh : IPartData
{
    public string partId => PartId;
    public int skinAmount => skinData.skinAmount;

    [SerializeField] string PartId;
    public SkinnedMeshRenderer skinnedMesh;
    public UnitSkinData skinData;

    public void ChangeSkin(int index)
    {
        skinData.ChangeSkin(skinnedMesh, index);
    }
    public void ChangeSkin(string skinId)
    {
        skinData.ChangeSkin(skinnedMesh, skinId);
    }

}
