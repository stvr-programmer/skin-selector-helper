using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PartData_Prefab : IPartData
{
    public string partId => PartId;
    public int skinAmount => skinData.skinAmount;

    [SerializeField] string PartId;
    public Transform parent;
    public UnitPrefabsSkinData skinData;

    Dictionary<string, GameObject> instantiateObjects = new Dictionary<string, GameObject>();
    GameObject activeSkin;


    public void ChangeSkin(int index)
    {
        string selectSkinId = skinData.GetSkinIdByIndex(index);
        ChangeSkin(selectSkinId);
    }
    public void ChangeSkin(string skinId)
    {
        if (activeSkin != null) activeSkin.SetActive(false);

        if (!instantiateObjects.ContainsKey(skinId))
        {
            GameObject targetSkin = skinData.ChangeSkin(skinId, parent);
            if (targetSkin == null) return;

            activeSkin = targetSkin;
            instantiateObjects.Add(skinId, targetSkin);

            return;
        }
        activeSkin = instantiateObjects[skinId];
        activeSkin.SetActive(true);
    }
}

