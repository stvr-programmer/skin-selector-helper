using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = " Skin Prefab Data", fileName = "_Skin")]
public class UnitPrefabsSkinData : ScriptableObject
{
    [SerializeField] List<PrefabsSkinData> unitSkinParts = new List<PrefabsSkinData>();
    Dictionary<string, PrefabsSkinData> cacheSkin = new Dictionary<string, PrefabsSkinData>();
    public int skinAmount { get { return unitSkinParts.Count; } }
    public GameObject ChangeSkin(string skinId, Transform ancor)
    {
        if (cacheSkin.Count <= 0) { InitialData(); }
        GameObject instantiateSkin;
        if (cacheSkin.ContainsKey(skinId))
        {
            instantiateSkin = Instantiate(cacheSkin[skinId].skinPrefab, ancor, false);
            instantiateSkin.transform.localPosition = cacheSkin[skinId].prefabsOffside;
            instantiateSkin.GetComponent<MeshRenderer>().sharedMaterials = cacheSkin[skinId].skinMaterials;

            return instantiateSkin;
        }
        return null;
    }
    public string GetSkinIdByIndex(int index)
    {
        return unitSkinParts[index].skinId;
    }
    private void InitialData()
    {
        foreach (var skin in unitSkinParts)
        {
            cacheSkin.Add(skin.skinId, skin);
        }
    }
}

[System.Serializable]
public struct PrefabsSkinData
{
    public string skinId;
    public Vector3 prefabsOffside;
    public GameObject skinPrefab;
    public Material[] skinMaterials;
}