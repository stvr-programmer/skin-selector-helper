using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectSkinData : MonoBehaviour
{
    public SkinType currentSkinType;
    [SerializeField] List<PartData_SkinnedMesh> skinnedMeshSkins = new List<PartData_SkinnedMesh>();
    [SerializeField] List<PartData_Prefab> prefabSkins = new List<PartData_Prefab>();

    public List<PartData_SkinnedMesh> GetSkinnedMeshSkinData()
    {
        return skinnedMeshSkins;
    }
    public List<PartData_Prefab> GetPrefabsSkinData()
    {
        return prefabSkins;
    }
    
    public void LoadSkin(SavedSkinData savedData)
    {
        List<IPartData> selectedData;
        SkinType skinType = savedData.skinType;
        switch (skinType)
        {
            case SkinType.skinned:
                selectedData = new List<IPartData>(skinnedMeshSkins);
                break;

            default:
                selectedData = new List<IPartData>(prefabSkins);
                break;
        }

        for (int i = 0; i < savedData.partId.Count; i++)
        {
            selectedData.Find(x => x.partId == savedData.partId[i]).ChangeSkin(savedData.skinCurrentIndex[i]);
        }
    }
}
