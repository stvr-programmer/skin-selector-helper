using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPartData
{
    string partId { get; }
    int skinAmount { get; }

    public void ChangeSkin(int index);
    public void ChangeSkin(string skinId);
}
