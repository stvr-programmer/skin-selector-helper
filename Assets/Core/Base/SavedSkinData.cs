using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class SavedSkinData
{
    public SkinType skinType;
    public List<string> partId = new List<string>();
    public List<int> skinCurrentIndex = new List<int>();

    public SavedSkinData(Dictionary<string, int> currentSkin, SkinType skinType)
    {
        this.skinType = skinType;
        foreach (var skin in currentSkin)
        {
            partId.Add(skin.Key);
            skinCurrentIndex.Add(skin.Value);
        }
    }
    public SavedSkinData(SavedSkinData newData)
    {
        this.skinType = newData.skinType;
        this.partId = newData.partId;
        this.skinCurrentIndex = newData.skinCurrentIndex;
    }
}
public enum SkinType
{
    skinned,
    prefab
}


