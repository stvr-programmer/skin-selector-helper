using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SkinSlector<T> : MonoBehaviour where T : IPartData
{
    private SkinType skinType;
    private List<T> unitSkinParts = new List<T>(); 
    public T currentPart { protected set; get; }
    protected Dictionary<string, int> currentPartSkinIndex = new Dictionary<string, int>();
    protected int currentChangeIndex = 0;

    internal void InitialData(List<T> skinData, SkinType skinType)
    {
        this.skinType = skinType;
        if (unitSkinParts.Count <= 0)
        {
            unitSkinParts = skinData;
        }
    }
    protected virtual string saveSkinCode { get { return "saveCode"; } }

    public void SetSelectedPartId(string partId)
    {
        if (currentPart == null || currentPart.partId != partId)
        {
            currentPart = unitSkinParts.Find(x => x.partId == partId);
        }
    }
    public void ChangeSkin(int incrementalChange)
    {
        int targetIndex = (currentChangeIndex + incrementalChange) % currentPart.skinAmount;
        currentChangeIndex = targetIndex < 0 ? currentPart.skinAmount - 1 : targetIndex;

        currentPart.ChangeSkin(currentChangeIndex);

        if (!currentPartSkinIndex.ContainsKey(currentPart.partId))
        {
            currentPartSkinIndex.Add(currentPart.partId, currentChangeIndex);

        }
        else { currentPartSkinIndex[currentPart.partId] = currentChangeIndex; }

    }

    private string GetCurrentSkinSet()
    {
        string json = JsonUtility.ToJson(new SavedSkinData(currentPartSkinIndex, skinType));
        Debug.Log("Saved JSON :" + json);
        return json;
    }
    private void SetCurrentSkin(string JSONData)
    {
        SavedSkinData savedData = new SavedSkinData(JsonUtility.FromJson<SavedSkinData>(JSONData));
        for (int i = 0; i < savedData.partId.Count; i++)
        {
            Debug.Log("Load : " + savedData.partId[i] + "/" + savedData.skinCurrentIndex[i]);
            SetSelectedPartId(savedData.partId[i]);
            currentPart.ChangeSkin(savedData.skinCurrentIndex[i]);
            currentChangeIndex = savedData.skinCurrentIndex[i];
        }

    }


    public virtual void SaveSkin()
    {
        PlayerPrefs.SetString(saveSkinCode, GetCurrentSkinSet());
    }
    public virtual void LoadSkin()
    {
        SetCurrentSkin(PlayerPrefs.GetString(saveSkinCode));
    }
}
