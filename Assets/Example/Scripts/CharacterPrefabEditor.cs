using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterPrefabEditor : SkinSlector<PartData_Prefab>
{
    public ObjectSkinData target;
    protected override string saveSkinCode => "prefabSkin";

    public void Initial()
    {
        base.InitialData(target.GetPrefabsSkinData(),target.currentSkinType);
    }
}
