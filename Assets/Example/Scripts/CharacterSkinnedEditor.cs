using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterSkinnedEditor : SkinSlector<PartData_SkinnedMesh>
{
    public ObjectSkinData target;
    protected override string saveSkinCode => "skinnedSkin";

    public void Initial()
    {
        base.InitialData(target.GetSkinnedMeshSkinData(), target.currentSkinType);
    }
}
