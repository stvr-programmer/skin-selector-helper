using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skin_SaverLoader : MonoBehaviour
{
    [SerializeField]ObjectSkinData prefabSkinObject;
    string prefabSaveCode => "prefabSkin";

    [SerializeField]ObjectSkinData skinnedMeshSkinObject;
    string saveSkinCode => "skinnedSkin";

    private void Start()
    {
        prefabSkinObject.LoadSkin(new SavedSkinData(JsonUtility.FromJson<SavedSkinData>(
            PlayerPrefs.GetString(prefabSaveCode)
            )));

        skinnedMeshSkinObject.LoadSkin(new SavedSkinData(JsonUtility.FromJson<SavedSkinData>(
           PlayerPrefs.GetString(saveSkinCode)
           )));
    }



}
